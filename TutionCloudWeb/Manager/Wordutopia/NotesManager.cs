﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Repository.Wordutopia;

namespace TutionCloudWeb.Manager.Wordutopia
{
    public class NotesManager
    {
        NotesRepository NotesRepository = new NotesRepository();
        /// <summary>
        /// Name : Notes
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<Notes> Notes(Notes model)
        {
            model = await NotesRepository.Notes(model);
            return model;
        }
        /// <summary>
        /// Name : NoteCount
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        /// 
        public string NoteCount(long userId, string searchText)
        {
            var results =  NotesRepository.NoteCount(userId, searchText);
            return results;
        }
        /// <summary>
        /// Name : deletNote
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        internal Tuple<bool, string> deletNote(string id)
        {
            var results = NotesRepository.deletNote(id);
            return results;
        }
    }
}