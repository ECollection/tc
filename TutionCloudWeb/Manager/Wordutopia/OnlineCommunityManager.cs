﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TutionCloudWeb.Models.Database;
using TutionCloudWeb.Repository.Wordutopia;

namespace TutionCloudWeb.Manager.Wordutopia
{
   
    public class OnlineCommunityManager
    {
        TutionCloudEntities _context = new TutionCloudEntities();
        internal TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn SendLIsts(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var returnModel = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            try
            {
                var user = _context.tUsers.FirstOrDefault(z => z.UserId == model.userId && z.IsActive && z.IsSuspended == false);
                if (user != null)
                {
                    var currentTime = System.DateTime.UtcNow;
                    string SendUserId = model.SendUserId;
                    string[] SendUserIdArry = SendUserId.Split(',');
                    //int counting = 0;
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();
                            string wordIds = model.wordIds;
                            string[] values = wordIds.Split(',');
                            //int count = 0;
                            for (int j = 0; j < values.Length; j++)
                            {
                                var wordId = Convert.ToInt64(values[j].ToString());

                                values[j] = values[j].Trim();

                                    var SendUser = _context.CreateAWordListToShares.Create();
                                    SendUser.CreatedOn = currentTime.ToString();
                                    SendUser.UserID = SendUserIdArry[i].ToString();
                                    SendUser.IsActive = true;
                                    SendUser.Words = wordId.ToString();
                                    SendUser.Password = model.Password;
                                    SendUser.TestID = model.TestID;
                                    SendUser.LastName = model.ListName;
                                    //SendUser.UserName = model.UserName;

                                    _context.CreateAWordListToShares.Add(SendUser);
                                    _context.SaveChanges();


                                 

                            }
                       returnModel.message = "Successful";
                    }
                    
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid user";
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }


    }
}