﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Repository.Wordutopia;

namespace TutionCloudWeb.Manager.Wordutopia
{
    public class UserManager
    {
        UserRepository UserRepository = new UserRepository();
        public async Task<bool> VerifyEmail(UserModel model)
        {
            var result = await UserRepository.VerifyEmail(model);
            return result;
        }

        internal TutionCloudWeb.Models.PostModel.User.ForgotpasswordReturn SendEmail(string subject, string mailbody, string reciever)
        {
            var returnModel = new TutionCloudWeb.Models.PostModel.User.ForgotpasswordReturn();
            returnModel.status = false;
            returnModel.message = "failed";

            try
            {

                var status = false;

                SmtpClient client = new SmtpClient();
                string userName = "tutioncloud.noreply@gmail.com";
                string password = "wordutopia";
                string fromName = "TutionCloud";
                MailAddress address = new MailAddress(userName, fromName);


                MailMessage message = new MailMessage();
                message.To.Add(new MailAddress(reciever, "Receiver"));
                message.From = address;
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = mailbody;
                client.Host = "smtp.gmail.com";//ConfigurationManager.AppSettings["smptpserver"];
                client.Port = 587;//Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                //client.Port = 465;
                client.EnableSsl = true;
                client.UseDefaultCredentials = true;
                //client.UseDefaultCredentials = true;
                client.Credentials = new NetworkCredential(userName, password);
                try
                {
                    client.Send(message);

                }
                catch (Exception e)
                {
                    status = false;
                }
                returnModel.status = true;
                returnModel.message = "Success";
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }

        public int RandomNumber()
        {
            Random random = new Random();
            return random.Next(10000, 99999);
        }

        //public async Task<bool> SaveVerifyEmailDB(UserModel model)
        //{
        //    var result = await UserRepository.SaveVerifyEmailDB(model);
        //    return result;
        //}


    }
}