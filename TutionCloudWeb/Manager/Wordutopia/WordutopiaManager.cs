﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Repository.Wordutopia;

namespace TutionCloudWeb.Manager.Wordutopia
{
    public class WordutopiaManager
    {
        WordutopiaRepository WordutopiaRepository = new WordutopiaRepository();
        /// <summary>
        /// Name : Words
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<WordsModel> Words(WordsModel model)
        {
            model = await WordutopiaRepository.Words(model);
            return model;
        }
        /// <summary>
        /// Name : Addword
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<WordsModel> Addword(WordsModel model)
        {
            model = await WordutopiaRepository.Addword(model);
            return model;
        }

        /// <summary>
        /// Name : deletWord
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        internal Tuple<bool, string> deletWord(string id)
        {
            var result =  WordutopiaRepository.deletWord(id);
            return result;
        }
        /// <summary>
        /// Name : WordCount
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public string WordCount(long userId, string searchText)
        {
            var results = WordutopiaRepository.WordCount(userId, searchText);
            return results;
        }
        
    }
}