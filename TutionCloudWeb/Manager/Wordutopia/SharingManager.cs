﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Models.Database;
using TutionCloudWeb.Repository.Wordutopia;

namespace TutionCloudWeb.Manager.Wordutopia
{
   
    public class SharingManager
    {
        TutionCloudEntities _context = new TutionCloudEntities();
        internal TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn SendLIsts(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var returnModel = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            try
            {
                var user = _context.tUsers.FirstOrDefault(z => z.UserId == model.userId && z.IsActive && z.IsSuspended == false);
                if (user != null)
                {
                    var currentTime = System.DateTime.UtcNow;
                    string SendUserId = model.SendUserId;
                    string[] SendUserIdArry = SendUserId.Split(',');
                    //int counting = 0;
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();
                            string wordIds = model.wordIds;
                            string[] values = wordIds.Split(',');
                            //int count = 0;
                            for (int j = 0; j < values.Length; j++)
                            {
                                var wordId = Convert.ToInt64(values[j].ToString());
                                var WordIdDetails = _context.tWords.Where(z => z.WordId == wordId).FirstOrDefault();


                                values[j] = values[j].Trim();

                                    var SendUser = _context.CreateAWordListToShares.Create();
                                    SendUser.SentUserID = model.userId.ToString();
                                    SendUser.CreatedOn = currentTime.ToString();
                                    SendUser.UserID = SendUserIdArry[i].ToString();
                                    SendUser.IsActive = true;
                                    SendUser.WordId = wordId.ToString();
                                    
                                    SendUser.Password = model.Password;
                                    //string a1 = SendUser.UserID.ToString();
                                    SendUser.TestID = model.TestID; //CreateWordListMaxId(SendUser.UserID); //model.TestID;////////////////////////
                                    SendUser.LastName = model.ListName;

                                    SendUser.CreatedDate = WordIdDetails.CreatedDate;
                                    SendUser.Definition = WordIdDetails.Definition;
                                    SendUser.Guid = WordIdDetails.Guid;
                                    SendUser.Hint = WordIdDetails.Hint;
                                    SendUser.Phrase = WordIdDetails.Phrase;
                                    SendUser.Pronounciation = WordIdDetails.Pronounciation;
                                    SendUser.ShareOnline = WordIdDetails.ShareOnline;
                                    SendUser.Word = WordIdDetails.Word;
                                    
                                    //SendUser.UserName = model.UserName;

                                    _context.CreateAWordListToShares.Add(SendUser);
                                    _context.SaveChanges();


                                 

                            }
                            
                       returnModel.message = "Successful";
                    }

                    var TestIds = _context.CreateAWordListTestIDs.Create();
                    TestIds.UserID = model.userId.ToString();
                    TestIds.TestID = Convert.ToInt64(CreateWordListMaxId(TestIds.UserID.ToString()));
                    _context.CreateAWordListTestIDs.Add(TestIds);
                    _context.SaveChanges();
                    
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid user";
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }

        public List<string> GetAllWordListSharedwithMe_Dates(string value)
        {
            List<string> Lists = new List<string>();
            List<CreateAWordListToShareModel> li = new List<CreateAWordListToShareModel>();

            var results = _context.SP_GET_DISTINCT_CreateAWordListToShare(value).ToList();
            foreach (var a1 in results)
            {
                var b1 = _context.CreateAWordListToShares.Where(x => x.CreatedOn == a1 && x.UserID == value).FirstOrDefault();
                CreateAWordListToShareModel obj = new CreateAWordListToShareModel();
                obj.CreateAWordListToShareID = b1.CreateAWordListToShareID;
                obj.CreatedOn = b1.CreatedOn;
                //obj.Retrieve = b1.Retrieve;
                li.Add(obj);
            }
            li = li.OrderByDescending(q => q.CreateAWordListToShareID).ToList();
            foreach (var a2 in li)
            {
                string str = a2.CreatedOn.ToString();
                Lists.Add(str);
            }


            return Lists;
        }

        public List<string> GetAllWordListSharedwithMe_Dates_Test(string value)
        {
            List<string> Lists = new List<string>();
            List<CreateAWordListToShareModel> li = new List<CreateAWordListToShareModel>();

            var results = _context.SP_GET_DISTINCT_CreateAWordListToShare_Test(value).ToList();
            foreach (var a1 in results)
            {
                var b1 = _context.TestShareds.Where(x => x.CreatedOn == a1 && x.UserID == value).FirstOrDefault();
                CreateAWordListToShareModel obj = new CreateAWordListToShareModel();
                obj.CreateAWordListToShareID = b1.TestSharedID;
                obj.CreatedOn = b1.CreatedOn;
                //obj.Retrieve = b1.Retrieve;
                li.Add(obj);
            }
            li = li.OrderByDescending(q => q.CreateAWordListToShareID).ToList();
            foreach (var a2 in li)
            {
                string str = a2.CreatedOn.ToString();
                Lists.Add(str);
            }


            return Lists;
        }

        public List<CreateAWordListToShareModel> GetAllWordListSharedwithMe_DatesChecks(string value)
        {
            
            List<CreateAWordListToShareModel> li = new List<CreateAWordListToShareModel>();

            var results = _context.SP_GET_DISTINCT_CreateAWordListToShare(value).ToList();
            foreach (var a1 in results)
            {
                var b1 = _context.CreateAWordListToShares.Where(x => x.CreatedOn == a1 && x.UserID == value).FirstOrDefault();
                CreateAWordListToShareModel obj = new CreateAWordListToShareModel();
                obj.CreateAWordListToShareID = b1.CreateAWordListToShareID;
                obj.CreatedOn = b1.CreatedOn;
                obj.Retrieve = b1.Retrieve;
                li.Add(obj);
            }
            return li;
        }

        public List<CreateAWordListToShareModel> GetAllWordListSharedwithMe_DatesChecks_Test(string value)
        {

            List<CreateAWordListToShareModel> li = new List<CreateAWordListToShareModel>();

            var results = _context.SP_GET_DISTINCT_CreateAWordListToShare_Test(value).ToList();
            foreach (var a1 in results)
            {
                string a2 = a1.ToString();
                var b1 = _context.TestShareds.Where(x => x.CreatedOn == a2 && x.UserID == value).FirstOrDefault();
                CreateAWordListToShareModel obj = new CreateAWordListToShareModel();
                obj.CreateAWordListToShareID = b1.TestSharedID;
                obj.CreatedOn = b1.CreatedOn;
                obj.Retrieve = b1.Retrieve;
                li.Add(obj);
            }
            return li;
        }

        public List<string> GetAllWordListWord_ListSharedbyMe_Dates(string value)
        {
            List<string> stringList = new List<string>();
            var results = _context.SP_GET_DISTINCT_WordListSharedbyMe(value).ToList();
            return results;
        }

        public string CreateWordListMaxId(string id)
        {
            try
            {
                long? R1 = _context.SP_CREATEAWORDLISTMAXID(id).FirstOrDefault();
                if (R1 == null)
                {
                    R1 = 0;
                }
                long results = 1 + Convert.ToInt64(R1);
                return results.ToString();
            }
            catch(Exception ex){
                return "0";
            }
            
        }

        public CreateAWordListToShareModel GetAllWordListSharedwithMe(string dates)
        {
            var results = _context.CreateAWordListToShares.Where(z => z.CreatedOn == dates).ToList();
            CreateAWordListToShareModel CreateAWordListToShareModelList = new CreateAWordListToShareModel();

            List<CreateAWordListToShareModel> Temp1List = new List<CreateAWordListToShareModel>();
            List<CreateAWordListToShareModel> Temp2List = new List<CreateAWordListToShareModel>();
            foreach (var a1 in results)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                model.CreateAWordListToShareID = a1.CreateAWordListToShareID;
                model.SentUserID = a1.SentUserID;
                model.UserID = a1.UserID;
                long d1  =Convert.ToInt64(a1.UserID);
                var r1 = _context.tUsers.Where(z => z.UserId == d1).FirstOrDefault();
                model.UserName = r1.Username;
                model.WordId = a1.WordId;
                model.Password = a1.Password;
                model.TestID = a1.TestID;
                model.LastName = a1.LastName;
                model.IsActive = a1.IsActive;
                model.CreatedOn = a1.CreatedOn;
                model.Pronounciation = a1.Pronounciation;
                model.Phrase = a1.Phrase;
                model.Hint = a1.Hint;
                model.Definition = a1.Definition;
                model.ShareOnline = a1.ShareOnline;
                model.CreatedDate = a1.CreatedDate.ToString();
                model.UpdatedDate = a1.UpdatedDate.ToString();
                model.Guid = a1.Guid;
                model.Word = a1.Word;
                Temp1List.Add(model);
                
            }
            
            var results2 = _context.tUsers.Select(z => new { z.UserId, z.Username }).ToList();
            foreach (var a2 in results2)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                model.UserID = a2.UserId.ToString();
                model.UserName = a2.Username;
                Temp2List.Add(model);
                //CreateAWordListToShareModelList.GetAllUserLists.Add(model);
            }
            CreateAWordListToShareModelList.GetallItems = Temp1List;
            CreateAWordListToShareModelList.GetAllUserLists = Temp2List;
            return CreateAWordListToShareModelList;
        }

        internal TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn UpdateLIsts(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {

            var resu = _context.SP_DELETETABLE_FROM_ROW_WordListSharedbyMe(model.TestID);

            var returnModel = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            try
            {
                var user = _context.tUsers.FirstOrDefault(z => z.UserId == model.userId && z.IsActive && z.IsSuspended == false);
                if (user != null)
                {
                    var currentTime = System.DateTime.UtcNow;
                    string SendUserId = model.SendUserId;
                    string[] SendUserIdArry = SendUserId.Split(',');
                    //int counting = 0;
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();
                        string wordIds = model.wordIds;
                        string[] values = wordIds.Split(',');
                        //int count = 0;
                        for (int j = 0; j < values.Length; j++)
                        {
                            var wordId = Convert.ToInt64(values[j].ToString());
                            var WordIdDetails = _context.tWords.Where(z => z.WordId == wordId).FirstOrDefault();


                            values[j] = values[j].Trim();

                            var SendUser = _context.CreateAWordListToShares.Create();
                            SendUser.SentUserID = model.userId.ToString();
                            SendUser.CreatedOn = currentTime.ToString();
                            SendUser.UserID = SendUserIdArry[i].ToString();
                            SendUser.IsActive = true;
                            SendUser.WordId = wordId.ToString();

                            SendUser.Password = model.Password;
                            //string a1 = SendUser.UserID.ToString();
                            SendUser.TestID = model.TestID; //CreateWordListMaxId(SendUser.UserID); //model.TestID;////////////////////////
                            SendUser.LastName = model.ListName;

                            SendUser.CreatedDate = WordIdDetails.CreatedDate;
                            SendUser.Definition = WordIdDetails.Definition;
                            SendUser.Guid = WordIdDetails.Guid;
                            SendUser.Hint = WordIdDetails.Hint;
                            SendUser.Phrase = WordIdDetails.Phrase;
                            SendUser.Pronounciation = WordIdDetails.Pronounciation;
                            SendUser.ShareOnline = WordIdDetails.ShareOnline;
                            SendUser.Word = WordIdDetails.Word;

                            //SendUser.UserName = model.UserName;

                            _context.CreateAWordListToShares.Add(SendUser);
                            _context.SaveChanges();




                        }

                        returnModel.message = "Update Successful";
                    }

                    var TestIds = _context.CreateAWordListTestIDs.Create();
                    TestIds.UserID = model.userId.ToString();
                    TestIds.TestID = Convert.ToInt64(CreateWordListMaxId(TestIds.UserID.ToString()));
                    _context.CreateAWordListTestIDs.Add(TestIds);
                    _context.SaveChanges();

                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid user";
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }

        public string Sent_ShareAnArchivedTest(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass model)
        {
            try
            {
                
                long examId = Convert.ToInt64(model.examId);
                var ExamWords = _context.tExamWords.Where(z => z.ExamId == examId).ToList();
                long testId =  ShareAnArchivedTestID(model.userId.ToString());
                var currentTime = System.DateTime.UtcNow;
                var createTestID = _context.ShareAnArchivedTestIDs.Create();
                createTestID.UserID = model.userId.ToString() ;
                createTestID.TestID = testId;
                _context.ShareAnArchivedTestIDs.Add(createTestID);
                _context.SaveChanges();

                foreach (var a1 in ExamWords)
                {
                    var tWords = _context.tWords.Where(z => z.WordId == a1.WordId).FirstOrDefault();
                    string SendUserId = model.SendUserId; 
                    string[] SendUserIdArry = SendUserId.Split(',');
                    
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var results = _context.ShareAnArchivedTests.Create();
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();

                        results.WordId = a1.WordId;
                        results.ExamId = examId;
                        results.Guid = Guid.NewGuid(); 
                        results.UserId = model.userId;
                        results.CreatedOn = currentTime.ToString();

                        results.Word = tWords.Word;
                        results.Pronounciation = tWords.Pronounciation;
                        results.Phrase = tWords.Phrase;
                        results.Hint = tWords.Hint;
                        results.Definition = tWords.Definition;
                        results.IsActive = tWords.IsActive;
                        results.ShareOnline = tWords.ShareOnline;
                        results.CreatedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss tt");

                        results.Password = model.Password;
                        results.LastName = model.LastName;
                        results.SentUserID = SendUserIdArry[i];
                        results.TestID = testId.ToString();
                        _context.ShareAnArchivedTests.Add(results);
                        _context.SaveChanges();
                    }

                    
                    
                }


                return "sending successful";
            }
            catch (Exception ex)
            {
                return "sending failed";
            }
            
        }

        public long ShareAnArchivedTestID(string id)
        {
            try
            {
                long? R1 = _context.SP_ShareAnArchivedTestIDMAXID(id).FirstOrDefault();
                if (R1 == null)
                {
                    R1 = 0;
                }
                long results = 1 + Convert.ToInt64(R1);
                return results;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }
        public List<TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set> setSortedItems(TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set model)
        {

            List<TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set> obj_List = new List<Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set>();
            List<TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set> obj_List1 = new List<Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set>();
           
            var lists_TestsIds = _context.ShareAnArchivedTest_Dupremove_Get(model.UserId).ToList(); 
            long strUsr = model.UserId.Value;
            var lists_Shared1 = _context.ShareAnArchivedTest_SentUserID_Dupremove_Get(strUsr).ToList();
            
            if (lists_TestsIds != null)
            {
                foreach (string a1 in lists_TestsIds)
                {
                    TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set obj = new Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set();
                    var getCreateDate = _context.ShareAnArchivedTests.Where(z => z.UserId == model.UserId.Value && z.TestID == a1).FirstOrDefault();
                    obj.CreatedDate = getCreateDate.CreatedDate;
                    obj.TestID = a1;
                    obj.UserId = model.UserId;
                    obj_List.Add(obj);

                }

            }
            if (lists_Shared1 != null)
            {
                foreach (var a3 in lists_Shared1)
                {
                    string R1 = a3.ToString();
                    string R2 = model.UserId.ToString();
                    var B1 = _context.ShareAnArchivedTests.Where(Z => Z.TestID == R1 && Z.SentUserID == R2).FirstOrDefault();

                    
                        TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set obj = new Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set();
                        obj.CreatedDate = B1.CreatedDate;
                        obj.TestID = B1.TestID;
                        obj.UserId = model.UserId;
                        obj.SentUserID = B1.SentUserID;
                        obj_List.Add(obj);

                      

                }

                        
            }
            obj_List1 = obj_List.OrderByDescending(z => z.CreatedDate).ToList();

            return obj_List1;
        }


        public CreateAWordListToShareModel ShareanArchivedTest_Binding(SharingModel dates)
        {
            //DateTime dat = Convert.ToDateTime(dates);
            //if (dates.shared == 0)
            //{
            //    var results = _context.ShareAnArchivedTests.Where(z => z.CreatedDate == dates.CreatedOn).ToList();
            //}
            //else if (dates.shared == 1)
            //{
            //    var results = _context.ShareAnArchivedTests.Where(z => z.CreatedDate == dates.CreatedOn).ToList();
            //}

             var results = _context.ShareAnArchivedTests.Where(z => z.CreatedDate == dates.CreatedOn).ToList();

            CreateAWordListToShareModel CreateAWordListToShareModelList = new CreateAWordListToShareModel();

            List<CreateAWordListToShareModel> Temp1List = new List<CreateAWordListToShareModel>();
            List<CreateAWordListToShareModel> Temp2List = new List<CreateAWordListToShareModel>();
            foreach (var a1 in results)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                //model.CreateAWordListToShareID = a1.CreateAWordListToShareID;
                model.SentUserID = a1.SentUserID;
                model.UserID = a1.SentUserID.ToString();
                long d1 = Convert.ToInt64(a1.SentUserID);
                var r1 = _context.tUsers.Where(z => z.UserId == d1).FirstOrDefault();
                model.UserName = r1.Username;
                model.WordId = a1.WordId.ToString();
                model.Password = a1.Password;
                model.TestID = a1.TestID;
                model.LastName = a1.LastName;
                model.IsActive = a1.IsActive;
                model.CreatedOn = a1.CreatedDate.ToString();
                model.Pronounciation = a1.Pronounciation;
                model.Phrase = a1.Phrase;
                model.Hint = a1.Hint;
                model.Definition = a1.Definition;
                model.ShareOnline = a1.ShareOnline;
                model.CreatedDate = a1.CreatedDate;
                model.UpdatedDate = a1.UpdatedDate;
                model.Guid = a1.Guid;
                model.Word = a1.Word;
                Temp1List.Add(model);

            }

            var results2 = _context.tUsers.Select(z => new { z.UserId, z.Username }).ToList();
            foreach (var a2 in results2)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                model.UserID = a2.UserId.ToString();
                model.UserName = a2.Username;
                Temp2List.Add(model);
                //CreateAWordListToShareModelList.GetAllUserLists.Add(model);
            }
            CreateAWordListToShareModelList.GetallItems = Temp1List;
            CreateAWordListToShareModelList.GetAllUserLists = Temp2List;
            return CreateAWordListToShareModelList;
        }


        internal TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn TestSharedSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var returnModel = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            try
            {
                var user = _context.tUsers.FirstOrDefault(z => z.UserId == model.userId && z.IsActive && z.IsSuspended == false);
                if (user != null)
                {
                    var currentTime = System.DateTime.UtcNow;
                    string UserIdString = model.userId.ToString();
                    string SendUserId = model.SendUserId;
                    string[] SendUserIdArry = SendUserId.Split(',');
                    SendUserIdArry = SendUserIdArry.Where(x => x != UserIdString).ToArray(); //removing Current UserId
                    //int counting = 0;
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();
                        string wordIds = model.wordIds;
                        string[] values = wordIds.Split(',');
                        values = values.Distinct().ToArray(); //removing Duplicate...
                        //int count = 0;
                        for (int j = 0; j < values.Length; j++)
                        {
                            var wordId = Convert.ToInt64(values[j].ToString());
                            var WordIdDetails = _context.tWords.Where(z => z.WordId == wordId).FirstOrDefault();


                            values[j] = values[j].Trim();

                            var SendUser = _context.TestShareds.Create();
                            SendUser.SentUserID = model.userId.ToString();
                            SendUser.CreatedOn = currentTime.ToString();
                            SendUser.UserID = SendUserIdArry[i].ToString();
                            SendUser.IsActive = true;
                            SendUser.WordId = wordId.ToString();

                            SendUser.Password = model.Password;
                            //string a1 = SendUser.UserID.ToString();
                            SendUser.TestID = model.TestID; //CreateWordListMaxId(SendUser.UserID); //model.TestID;////////////////////////
                            SendUser.LastName = model.ListName;

                            SendUser.CreatedDate = WordIdDetails.CreatedDate;
                            SendUser.Definition = WordIdDetails.Definition;
                            SendUser.Guid = WordIdDetails.Guid;
                            SendUser.Hint = WordIdDetails.Hint;
                            SendUser.Phrase = WordIdDetails.Phrase;
                            SendUser.Pronounciation = WordIdDetails.Pronounciation;
                            SendUser.ShareOnline = WordIdDetails.ShareOnline;
                            SendUser.Word = WordIdDetails.Word;

                            //SendUser.UserName = model.UserName;

                            _context.TestShareds.Add(SendUser);
                            _context.SaveChanges();




                        }

                        returnModel.message = "Successful";
                    }

                    //var TestIds = _context.CreateAWordListTestIDs.Create();
                    //TestIds.UserID = model.userId.ToString();
                    //TestIds.TestID = Convert.ToInt64(CreateWordListMaxId(TestIds.UserID.ToString()));
                    //_context.CreateAWordListTestIDs.Add(TestIds);
                    //_context.SaveChanges();

                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid user";
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }


        public CreateAWordListToShareModel GetAllWordListTestSharedwithMe(string dates)
        {
            var results = _context.TestShareds.Where(z => z.CreatedOn == dates).ToList();
            CreateAWordListToShareModel CreateAWordListToShareModelList = new CreateAWordListToShareModel();

            List<CreateAWordListToShareModel> Temp1List = new List<CreateAWordListToShareModel>();
            List<CreateAWordListToShareModel> Temp2List = new List<CreateAWordListToShareModel>();
            foreach (var a1 in results)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                model.CreateAWordListToShareID = a1.TestSharedID;
                model.SentUserID = a1.SentUserID;
                model.UserID = a1.UserID;
                long d1 = Convert.ToInt64(a1.UserID);
                var r1 = _context.tUsers.Where(z => z.UserId == d1).FirstOrDefault();
                model.UserName = r1.Username;
                model.WordId = a1.WordId;
                model.Password = a1.Password;
                model.TestID = a1.TestID;
                model.LastName = a1.LastName;
                model.IsActive = a1.IsActive;
                model.CreatedOn = a1.CreatedOn;
                model.Pronounciation = a1.Pronounciation;
                model.Phrase = a1.Phrase;
                model.Hint = a1.Hint;
                model.Definition = a1.Definition;
                model.ShareOnline = a1.ShareOnline;
                model.CreatedDate = a1.CreatedDate.ToString();
                model.UpdatedDate = a1.UpdatedDate.ToString();
                model.Guid = a1.Guid;
                model.Word = a1.Word;
                Temp1List.Add(model);

            }

            var results2 = _context.tUsers.Select(z => new { z.UserId, z.Username }).ToList();
            foreach (var a2 in results2)
            {
                CreateAWordListToShareModel model = new CreateAWordListToShareModel();
                model.UserID = a2.UserId.ToString();
                model.UserName = a2.Username;
                Temp2List.Add(model);
                //CreateAWordListToShareModelList.GetAllUserLists.Add(model);
            }
            CreateAWordListToShareModelList.GetallItems = Temp1List;
            CreateAWordListToShareModelList.GetAllUserLists = Temp2List;
            return CreateAWordListToShareModelList;
        }


        internal TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn UpdateaWordListToTestSharedbyMeSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {

            var resu = _context.SP_DELETETABLE_FROM_ROW_WordListTestSharedbyMe(model.TestID);

            var returnModel = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWordReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            try
            {
                var user = _context.tUsers.FirstOrDefault(z => z.UserId == model.userId && z.IsActive && z.IsSuspended == false);
                if (user != null)
                {
                    var currentTime = System.DateTime.UtcNow;
                    string SendUserId = model.SendUserId;
                    string[] SendUserIdArry = SendUserId.Split(',');
                    //int counting = 0;
                    for (int i = 0; i < SendUserIdArry.Length; i++)
                    {
                        var SendUserIdVar = Convert.ToInt64(SendUserIdArry[i].ToString());
                        SendUserIdArry[i] = SendUserIdArry[i].Trim();
                        string wordIds = model.wordIds;
                        string[] values = wordIds.Split(',');
                        //int count = 0;
                        for (int j = 0; j < values.Length; j++)
                        {
                            var wordId = Convert.ToInt64(values[j].ToString());
                            var WordIdDetails = _context.tWords.Where(z => z.WordId == wordId).FirstOrDefault();


                            values[j] = values[j].Trim();

                            var SendUser = _context.TestShareds.Create();
                            SendUser.SentUserID = model.userId.ToString();
                            SendUser.CreatedOn = currentTime.ToString();
                            SendUser.UserID = SendUserIdArry[i].ToString();
                            SendUser.IsActive = true;
                            SendUser.WordId = wordId.ToString();

                            SendUser.Password = model.Password;
                            //string a1 = SendUser.UserID.ToString();
                            SendUser.TestID = model.TestID; //CreateWordListMaxId(SendUser.UserID); //model.TestID;////////////////////////
                            SendUser.LastName = model.ListName;

                            SendUser.CreatedDate = WordIdDetails.CreatedDate;
                            SendUser.Definition = WordIdDetails.Definition;
                            SendUser.Guid = WordIdDetails.Guid;
                            SendUser.Hint = WordIdDetails.Hint;
                            SendUser.Phrase = WordIdDetails.Phrase;
                            SendUser.Pronounciation = WordIdDetails.Pronounciation;
                            SendUser.ShareOnline = WordIdDetails.ShareOnline;
                            SendUser.Word = WordIdDetails.Word;

                            //SendUser.UserName = model.UserName;

                            _context.TestShareds.Add(SendUser);
                            _context.SaveChanges();




                        }

                        returnModel.message = "Update Successful";
                    }

                    //var TestIds = _context.CreateAWordListTestIDs.Create();
                    //TestIds.UserID = model.userId.ToString();
                    //TestIds.TestID = Convert.ToInt64(CreateWordListMaxId(TestIds.UserID.ToString()));
                    //_context.CreateAWordListTestIDs.Add(TestIds);
                    //_context.SaveChanges();

                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid user";
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
        }

        public List<string> GetAllWordListWord_ListTestSharedbyMe_Dates(string value)
        {
            List<string> stringList = new List<string>();
            var results = _context.SP_GET_DISTINCT_WordListTestSharedbyMe(value).ToList();
            return results;
        }



    }
}