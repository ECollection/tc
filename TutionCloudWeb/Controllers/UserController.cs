﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Models.Repository;
using CaptchaMvc.HtmlHelpers;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using System.Threading.Tasks;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Manager.Wordutopia;

namespace TutionCloudWeb.Controllers
{
    public class UserController : PreLoginBaseController
    {
        public UserManager UserManager = new UserManager();
        private userRepository _userRepo = new userRepository();
        //
        // GET: /User/
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult register(TutionCloudWeb.Models.PostModel.User.Register model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                if (!this.IsCaptchaValid("Captcha is not valid"))
                {
                    status = false;
                    message = "Captcha Invalid";
                    return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = _userRepo.register(model);
                    return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult refreshCaptche()
        {
            return PartialView("~/Views/User/_pv_captcha.cshtml");
        }


        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult login(TutionCloudWeb.Models.PostModel.User.Login model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.login(model);
                return Json(new { status = result.status, message = result.message, userid = result.userid }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

        //[HttpPost]
        //public ActionResult SendEmail(string receiver, string message)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {

        //            var senderEmail = new MailAddress("tutioncloud.noreply@gmail.com", "wordutopia");
        //            var receiverEmail = new MailAddress(receiver, "Receiver");
        //            var password = "wordutopia";
        //            var sub = "Password Change";
        //            var body = message;
        //            var smtp = new SmtpClient
        //            {
        //                Host = "smtp.gmail.com",
        //                Port = 587,
        //                EnableSsl = true,
        //                DeliveryMethod = SmtpDeliveryMethod.Network,
        //                UseDefaultCredentials = false,
        //                Credentials = new NetworkCredential(senderEmail.Address, password)
        //            };
        //            using (var mess = new MailMessage(senderEmail, receiverEmail)
        //            {
        //                Subject = sub,
        //                Body = body
        //            })
        //            {
        //                smtp.Send(mess);
        //            }
        //            return View("~/Views/User/Login.cshtml");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.Error = "Some Error";
        //    }
        //    return View();
        //}





        [HttpPost]
        public ActionResult SendEmail(TutionCloudWeb.Models.PostModel.User.Forgotpassword model1)
        {
            TutionCloudWeb.Models.PostModel.User.Forgotpassword model = new TutionCloudWeb.Models.PostModel.User.Forgotpassword();
            model.reciever = model1.reciever;
            bool status = true;
            string messages = "success";

            var user = _context.tUsers.Where(z => z.email == model1.reciever.Trim() && z.IsActive).FirstOrDefault();
            if (user != null)
            {
                var existemail = _context.tForgotPasswords.Where(z => z.userid == user.UserId && z.status).FirstOrDefault();
                if (existemail == null)
                {
                    model.userid = user.UserId;
                    var result = _userRepo.forgotpass(model);
                    if (result.status)
                    {
                        //model.message = "http://localhost:63019/User/PasswordChange";
                        model.message = "http://202.191.65.67:4139/User/PasswordChange";
                        _userRepo.sendemail(model);

                        status = true;
                        messages = "message";
                        return Json(new { status = status, message = messages }, JsonRequestBehavior.AllowGet);

                        //  return View("~/Views/User/Login.cshtml");
                    }
                    else
                    {
                        status = false;
                        messages = "failed";
                        return Json(new { status = status, message = messages }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    status = false;
                    messages = "Link already exists";
                    return Json(new { status = status, message = messages }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

                status = false;
                messages = "Please enter registered mail id";
                return Json(new { status = status, message = messages }, JsonRequestBehavior.AllowGet);
            }


        }
        public ActionResult PasswordChange()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult passwordchange(TutionCloudWeb.Models.PostModel.User.Forgotpassword model)
        {
            bool status = true;
            string message = "success";

            var result = _userRepo.Updatepassword(model);
            return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);

        }

        //
        /// <summary>
        /// VerifyEmail
        /// </summary>
        /// <param name=></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> VerifyEmail(string value)
        {
            UserModel model = new UserModel();
            model.Email = value;
            //bool results = await UserManager.VerifyEmail(model);
            //if (results == false)
            //{
                
                var filePath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/Views/User/_pv_VerifyEmail.cshtml");
                var emailTemplate = System.IO.File.ReadAllText(filePath);
                model.RandomNumber = UserManager.RandomNumber().ToString();

                //bool results2 = await UserManager.SaveVerifyEmailDB(model);

                var mBody = emailTemplate.Replace("verificationCode", model.RandomNumber);                
                var result = UserManager.SendEmail("Forgot Password", mBody, model.Email);                

            //}
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult _pv_VerifyEmail(UserModel model)
        {
            return null;
        }


    }
}