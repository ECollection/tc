﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Models.Repository;

namespace TutionCloudWeb.Controllers
{
    public class WordutopiaController : BaseController
    {
        private userRepository _userRepo = new userRepository();
        //
        // GET: /Wordutopia/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }

        public ActionResult Words()
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public ActionResult CreateWord()
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.AddWord();
            model.userId = _User.UserId;
            model.sharOnline = true;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult addword(TutionCloudWeb.Models.PostModel.Word.AddWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addword(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult refreshUserWordList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserWordListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Word_List_Pagination.cshtml", model);
        }

        public object deletWord(string wordId)
        {
            bool status = false;
            string message = "failed";
            if (wordId != null && wordId != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deletWord(wordId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string WordCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FETCH_USER_TOTAL_WORD_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }


        public ActionResult Notes()
        {
            var model = new TutionCloudWeb.Models.PostModel.Note.NoteClass();
            model.userId = _User.UserId;
            model.totalNoteCount = _context.SP_FETCH_USER_TOTAL_NOTE_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserNoteList(TutionCloudWeb.Models.PostModel.Note.NoteList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Note_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserNoteListPagination(TutionCloudWeb.Models.PostModel.Note.NoteListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Note_List_Pagination.cshtml", model);
        }

        public string NoteCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FETCH_USER_TOTAL_NOTE_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public object deletNote(string noteId)
        {
            bool status = false;
            string message = "failed";
            if (noteId != null && noteId != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deletNote(noteId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateNote()
        {
            var model = new TutionCloudWeb.Models.PostModel.Note.AddNote();
            model.userId = _User.UserId;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult addnote(TutionCloudWeb.Models.PostModel.Note.AddNote model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addnote(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Favourites()
        {
            var model = new TutionCloudWeb.Models.PostModel.Favourites.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_FAVOURITE_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public ActionResult EditWord(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            string[] values = strOriginal.Split('-');
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = values[i].Trim();
            }

            var wordId = Convert.ToInt64(values[0]);
            var model = new TutionCloudWeb.Models.PostModel.Word.EditWord();
            model.redirectTo = values[1];
            model.mainPanelIndex = Convert.ToInt32(values[2]);
            model.userId = _User.UserId;
            model.wordId = wordId;

            var word = _context.SP_FETCH_WORD_BY_ID(wordId).ToList().Select(z => new TutionCloudWeb.Models.DataClass.SP_FETCH_WORD_BY_ID(z)).FirstOrDefault();
            if (word != null)
            {
                model.sharOnline = word.SHAREONLINE;
                model.definition = word.DEFINITION;
                model.hint = word.HINT;
                model.phrase = word.PHRASE;
                model.pronounciation = word.PRONOUNCIATION;
                model.word = word.WORD;
                return View(model);
            }
            else
            {
                //return HttpNotFound();
                return RedirectToAction("Words");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult editword(TutionCloudWeb.Models.PostModel.Word.EditWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.editword(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditNote(string id)
        {
            var noteId = Convert.ToInt64(id);
            var model = new TutionCloudWeb.Models.PostModel.Note.EditNote();
            model.userId = _User.UserId;
            model.noteId = noteId;

            var word = _context.SP_FETCH_NOTE_BY_ID(noteId).ToList().Select(z => new TutionCloudWeb.Models.DataClass.SP_FETCH_NOTE_BY_ID(z)).FirstOrDefault();
            if (word != null)
            {
                model.title = word.TITLE;
                model.description = word.DESCRIPTION;
                return View(model);
            }
            else
            {
                return RedirectToAction("Notes");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult editnote(TutionCloudWeb.Models.PostModel.Note.EditNote model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.editnote(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult MainPanel(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.MainPanel.WordList();
            model.userId = _User.UserId;
            model.index = Convert.ToInt32(strOriginal);
            model.prevIndex = Convert.ToInt32(1);
            model.isNewRandom = true;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_MAINPANEL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();

            //28-02-2019 premjith


            Random rnd = new Random();

            if (model.isNewRandom)
            {
                int r = model.prevIndex;
                if (model.index <= 0)
                {
                    while ((r == model.prevIndex) || (r == model.index))
                    {
                        r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                    }
                }
                else
                {
                    if (r == model.index)
                    {
                        while ((r == model.index) || (r == model.prevIndex))
                        {
                            r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                        }
                    }
                    else
                    {
                        while ((r == model.prevIndex) || (r == model.index))
                        {
                            r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                        }
                    }
                }
                model.index = r;
            }

            var word = new TutionCloudWeb.Models.DataClass.WebService().getUserWordByRowNumber(model.userId, model.index);
            if (word != null)
            {
                var sessionValue = Convert.ToString(Session["WordIds"]);
                if (sessionValue != null && sessionValue != string.Empty)
                {
                    if (!sessionValue.Split(',').Contains(Convert.ToString(word.WORDID)))
                    {
                        var newSessionValue = sessionValue + "," + Convert.ToString(word.WORDID);
                        Session["WordIds"] = Convert.ToString(newSessionValue);
                    }
                }
                else
                {
                    Session["WordIds"] = Convert.ToString(word.WORDID);
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserMainPanelList(TutionCloudWeb.Models.PostModel.MainPanel.WordList model)
        {
            var value = Convert.ToBoolean(Session["showAllWords"] ?? false);
            if (value)
            {
                Session["WordIds"] = null;
            }
            if (model.isNewRandom)
            {
                Session["showAllWords"] = false;
            }
            Random rnd = new Random();

            if (model.isNewRandom)
            {
                int r = model.prevIndex;
                if (model.index <= 0)
                {
                    while ((r == model.prevIndex) || (r == model.index))
                    {
                        r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                    }
                }
                else
                {
                    if (r == model.index)
                    {
                        while ((r == model.index) || (r == model.prevIndex))
                        {
                            r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                        }
                    }
                    else
                    {
                        while ((r == model.prevIndex) || (r == model.index))
                        {
                            r = rnd.Next(1, Convert.ToInt32(Convert.ToInt32(model.totalWordCount)) + Convert.ToInt32(1));
                        }
                    }
                }
                model.index = r;
            }

            var word = new TutionCloudWeb.Models.DataClass.WebService().getUserWordByRowNumber(model.userId, model.index);
            if (word != null)
            {
                var sessionValue = Convert.ToString(Session["WordIds"]);
                if (sessionValue != null && sessionValue != string.Empty)
                {
                    if (!sessionValue.Split(',').Contains(Convert.ToString(word.WORDID)))
                    {
                        var newSessionValue = sessionValue + "," + Convert.ToString(word.WORDID);
                        Session["WordIds"] = Convert.ToString(newSessionValue);
                    }
                }
                else
                {
                    Session["WordIds"] = Convert.ToString(word.WORDID);
                }
            }


            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Mainpanel_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserFavouriteWordList(TutionCloudWeb.Models.PostModel.Favourites.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Favourite_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserFavouriteWordListPagination(TutionCloudWeb.Models.PostModel.Favourites.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Favourite_Word_List_Pagination.cshtml", model);
        }

        [HttpPost]
        public ActionResult addfavouriteword(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addfavouriteword(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public string FavouriteWordCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FETCH_USER_TOTAL_FAVOURITE_WORD_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public object deletWordFromFavourites(string wordId)
        {
            bool status = false;
            string message = "failed";
            if (wordId != null && wordId != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deletWordFromFavourites(wordId, _User.UserId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SkipList()
        {
            var model = new TutionCloudWeb.Models.PostModel.SkipList.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_SKIPPED_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserSkipListWordList(TutionCloudWeb.Models.PostModel.SkipList.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_SkipList_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserSkipListWordListPagination(TutionCloudWeb.Models.PostModel.SkipList.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_SkipList_Word_List_Pagination.cshtml", model);
        }

        public string SkipListWordCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FETCH_USER_TOTAL_SKIPPED_WORD_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public object deletWordFromSkipList(string wordId)
        {
            bool status = false;
            string message = "failed";
            if (wordId != null && wordId != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deletWordFromSkipList(wordId, _User.UserId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult addskippedword(TutionCloudWeb.Models.PostModel.Word.AddSkippedWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addskippedword(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReverseTest(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.ReverseTest.WordList();
            model.userId = _User.UserId;
            model.index = Convert.ToInt32(strOriginal);
            model.prevIndex = Convert.ToInt32(1);
            model.isNewRandom = true;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_MAINPANEL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            //Random rnd = new Random();
            //int r = Convert.ToInt32(strOriginal);
            //while (r != Convert.ToInt32(strOriginal))
            //{
            //    r = rnd.Next(1, Convert.ToInt32(model.totalWordCount));
            //}
            //model.index = Convert.ToInt32(r);

            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserReversetestList(TutionCloudWeb.Models.PostModel.ReverseTest.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_ReverseTest_List.cshtml", model);
        }

        public ActionResult Exams()
        {
            var model = new TutionCloudWeb.Models.PostModel.Exam.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_TOTAL_WORDLIST_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserExamWordList(TutionCloudWeb.Models.PostModel.Exam.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Exam_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserExamWordListPagination(TutionCloudWeb.Models.PostModel.Exam.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_Exam_Word_List_Pagination.cshtml", model);
        }

        public string ExamWordCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FETCH_TOTAL_WORDLIST_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public ActionResult CreateTest(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.Exam.CreateTest();
            model.testTime = "";
            model.timePerWord = "";
            model.title = "";
            model.userId = _User.UserId;
            model.wordIds = strOriginal;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult createtest(TutionCloudWeb.Models.PostModel.Exam.CreateTest model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.createtest(model);
                return Json(new { status = result.status, message = result.message, id = result.examId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StartTest(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.Exam.StartTest();
            model.userId = _User.UserId;
            model.examId = strOriginal;
            var examId = Convert.ToInt64(strOriginal);
            var exam = _context.tExams.FirstOrDefault(z => z.ExamId == examId);
            if (exam != null)
            {
                model.testTime = exam.TestTime;
                model.timePerWord = exam.TimePerWord;
            }
            else
            {
                model.testTime = "0";
                model.timePerWord = "0";
            }
            return View(model);
        }

        public ActionResult ViewAnswer(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.Exam.ViewTestAnswer();
            model.userId = _User.UserId;
            model.wordIds = new List<long>();

            List<string> result = strOriginal.Split(',').ToList();
            if (result.Count > 0)
            {
                model.examId = result.FirstOrDefault();
                if (result.Count > 1)
                {
                    result.RemoveAt(0);
                    model.notviewedwordIds = "";
                    foreach (var item in result)
                    {
                        try
                        {
                            model.wordIds.Add(Convert.ToInt64(item));
                            if (model.notviewedwordIds == "")
                            {
                                model.notviewedwordIds = model.notviewedwordIds + item;
                            }
                            else
                            {
                                model.notviewedwordIds = model.notviewedwordIds + "," + item;
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                }
            }
            else
            {
                model.examId = "0";
            }


            //var examId = Convert.ToInt64(strOriginal);
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveResultToDb(TutionCloudWeb.Models.PostModel.Exam.SaveResult model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.SaveResultToDb(model);
                return Json(new { status = result.status, message = result.message, id = result.examId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TestSummary(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.Exam.TestSummary();
            model.userId = _User.UserId;
            model.examId = strOriginal;
            var examId = Convert.ToInt64(strOriginal);
            model.totalWordCount = Convert.ToString(_context.SP_FETCH_EXAM_WORDS(examId, _User.UserId).ToList().Count());
            return View(model);
        }


        [HttpPost]
        public ActionResult refreshTestSummaryWordList(TutionCloudWeb.Models.PostModel.Exam.TestSummaryWordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_TestSummary_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshTestSummaryWordListPagination(TutionCloudWeb.Models.PostModel.Exam.TestSummaryWordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_TestSummary_Word_List_Pagination.cshtml", model);
        }
        //basheer on 02/01/2019 for checking wordshare
        public string Sharecheck(string word)
        {
            bool status = true;
            string message = "success";
            try
            {
                var count = _context.SP_CHECK_WORDSHARED(word ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public string UserWordCount(int userid)
        {
            bool status = true;
            string message = "success";
            try
            {
                var count = _context.SP_CHECK_WORDSHARED_EDITWORD(userid).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }


        public ActionResult Crossword(string id)
        {
            //var result = _userRepo.createCrossword(id, _User.UserId);
            var populateModel = _userRepo.populateModel(id, _User.UserId);
            return View(populateModel);
        }

        public ActionResult OnlineCommunity()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = "11";
            model.totalWordCount = _context.SP_FECTH_ONLINE_COMMUNITY_WORDS_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
        public ActionResult OnlineCommunitys()
        {
            return View();
        }


        [HttpPost]
        public ActionResult deletSelectedWords(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deletSelectedWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deleteAllWords(TutionCloudWeb.Models.PostModel.Word.DeleteAllWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deleteAllWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult deletSelectedFavouriteWords(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedFavoutiteWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deletSelectedFavouriteWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deleteAllFavouriteWords(TutionCloudWeb.Models.PostModel.Word.DeleteAllFavoutiteWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deleteAllFavouriteWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deletSelectedSkiplistWords(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedSkiplistWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deletSelectedSkiplistWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deleteAllSkiplistWords(TutionCloudWeb.Models.PostModel.Word.DeleteAllSkiplistWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.deleteAllSkiplistWords(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult refreshOnlineCommunityWordList(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_OnlineCommunity_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshOnlineCommunityWordListPagination(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_OnlineCommunity_Word_List_Pagination.cshtml", model);
        }
        public string OnlineCommunityWordCount(long userId, string searchText)
        {
            try
            {
                var count = _context.SP_FECTH_ONLINE_COMMUNITY_WORDS_COUNT(userId, searchText ?? string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        [HttpPost]
        public object addWordToDatabase(TutionCloudWeb.Models.PostModel.OnlineCommunity.AddWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addWordToDatabase(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public object addAllWordToDatabase(TutionCloudWeb.Models.PostModel.OnlineCommunity.AddAllWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid && model.wordId != null)
            {
                var result = _userRepo.addAllWordToDatabase(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateCrossword()
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshGenerateCrosswordUserWordList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_GenrateCrossword_Word_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshGenerateCrosswordUserWordListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Wordutopia_GenerateCrossword_Word_List_Pagination.cshtml", model);
        }


        public ActionResult ReviewWords()
        {
            var value = Convert.ToBoolean(Session["showAllWords"] ?? false);
            var model = new TutionCloudWeb.Models.PostModel.ReviewWords.WordList();
            model.userId = _User.UserId;
            model.showAllWords = value;
            var mainPanelViewedWords = Convert.ToString(Session["WordIds"]);
            model.mainPanelReviewedtotalWordCount = 0;
            if (mainPanelViewedWords != null && mainPanelViewedWords != string.Empty)
            {
                string[] values = mainPanelViewedWords.Split(',');
                //for (int i = 0; i < values.Length; i++)
                //{
                //    values[i] = values[i].Trim();
                //}
                model.mainPanelReviewedtotalWordCount = values.Length;
            }
            model.mainPanelReviewedWordIDs = mainPanelViewedWords;

            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_WORD_COUNT(_User.UserId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }


        [HttpPost]
        public ActionResult refreshReviewWordList()
        {
            Session["showAllWords"] = true;
            var model = new TutionCloudWeb.Models.PostModel.ReviewWords.WordList();
            model.userId = _User.UserId;
            model.showAllWords = true;
            model.totalWordCount = _context.SP_FETCH_USER_TOTAL_WORD_COUNT(_User.UserId, string.Empty).FirstOrDefault().ToString();
            var mainPanelViewedWords = Convert.ToString(Session["WordIds"]);
            model.mainPanelReviewedtotalWordCount = 0;
            if (mainPanelViewedWords != null && mainPanelViewedWords != string.Empty)
            {
                string[] values = mainPanelViewedWords.Split(',');
                //for (int i = 0; i < values.Length; i++)
                //{
                //    values[i] = values[i].Trim();
                //}
                model.mainPanelReviewedtotalWordCount = values.Length;
            }
            model.mainPanelReviewedWordIDs = mainPanelViewedWords;
            return PartialView("~/Views/Wordutopia/_pv_table_review_words_list.cshtml", model);
        }


        [HttpPost]
        public ActionResult saveCrosswordToDB(TutionCloudWeb.Models.PostModel.Crossword.AddCrosswordToDB model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                model.userId = _User.UserId;
                var result = _userRepo.saveCrosswordToDB(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CrosswordArchives()
        {
            var model = new TutionCloudWeb.Models.PostModel.Crossword.CrosswordArchives();
            model.userId = _User.UserId;
            model.totalCrosswordCount = _context.SP_FETCH_USER_TOTAL_CROSSWORD_COUNT(_User.UserId).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserCrosswordList(TutionCloudWeb.Models.PostModel.Crossword.CrosswordArchivesList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Crossword_Archieves_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserCrosswordListPagination(TutionCloudWeb.Models.PostModel.Crossword.CrosswordArchivesListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Crossword_Archieves_List_Pagination.cshtml", model);
        }

        public object deleteCrossword(string id)
        {
            bool status = false;
            string message = "failed";
            if (id != null && id != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deleteCrossword(id);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public object deleteAllCrossword(string id)
        {
            bool status = false;
            string message = "failed";
            if (id != null && id != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deleteAllCrossword(id);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult TestArchives()
        {
            var model = new TutionCloudWeb.Models.PostModel.Exam.TestArchives();
            model.userId = _User.UserId;
            model.totalTestCount = _context.SP_FETCH_USER_TOTAL_TEST_COUNT(_User.UserId).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public ActionResult refreshUserTestList(TutionCloudWeb.Models.PostModel.Exam.TestArchivesList model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Test_Archives_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserTestListPagination(TutionCloudWeb.Models.PostModel.Exam.TestArchivesListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Test_Archives_List_Pagination.cshtml", model);
        }


        public object deleteTest(string id)
        {
            bool status = false;
            string message = "failed";
            if (id != null && id != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deleteTest(id);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        public object deleteAllTest(string id)
        {
            bool status = false;
            string message = "failed";
            if (id != null && id != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deleteAllTest(id);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        public object deletSelectedTest(string id)
        {
            bool status = true;
            string message = "success";
            if (id != null && id != string.Empty)
            {
                Tuple<bool, string> result = _userRepo.deletSelectedTest(id, _User.UserId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ResitTest(string id)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                Tuple<bool, string, string> result = _userRepo.resittest(id, _User.UserId);
                if (result.Item1)
                {
    //                return RedirectToAction("Main", new System.Web.Routing.RouteValueDictionary(
    //new { controller = "Wordutopia", action = "StartTest", Id = result.Item3 }));
                    byte[] byt2 = System.Text.Encoding.UTF8.GetBytes(result.Item3.ToString());
                    var strModified2 = Convert.ToBase64String(byt2);

                    return Redirect("/Wordutopia/StartTest/" + strModified2);
                }
                else
                {
                    return Json(new { status = result.Item1, message = result.Item2, id = result.Item3 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NoteDescription(string id)
        {
            var model = new TutionCloudWeb.Models.PostModel.Note.NoteDescription();
            model.Noteid = Convert.ToInt32(id);
            return View(model);
        }



        [HttpPost]
        public ActionResult deleteAllReviewedWords(TutionCloudWeb.Models.PostModel.Word.DeleteAllWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                Session["WordIds"] = null;
                return Json(new { status = true, message = "success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult deletSelectedReviewedWords(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var mainPanelViewedWords = Convert.ToString(Session["WordIds"]);
                //if (mainPanelViewedWords != null && mainPanelViewedWords != string.Empty)
                //{
                //    string[] values = mainPanelViewedWords.Split(',');
                //    for (int i = 0; i < values.Length; i++)
                //    {
                //        values[i] = values[i].Trim();
                //    }
                //    values
                //}
                List<String> Items = mainPanelViewedWords.Split(',').Select(i => i.Trim()).Where(i => i != string.Empty).ToList(); //Split them all and remove spaces
                List<String> Items1 = model.wordIds.Split(',').Select(i => i.Trim()).Where(i => i != string.Empty).ToList(); //Split them all and remove spaces
                foreach (var item in Items1)
                {
                    Items.Remove(item.ToString()); //or whichever you want
                }
                string newSessionValue = String.Join(", ", Items.ToArray());
                Session["WordIds"] = newSessionValue;
                return Json(new { status = true, message = "success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }
        //Basheer on 05/03/2019 for checking mail for forgot password
        public object checkemail(string id)
        {
            var emailid = id;
            bool status = false;
            string message = "success";
            var userdetails = _context.tUsers.Where(z => z.email == emailid && z.IsActive).FirstOrDefault();
            if (userdetails != null)
            {
                var usid = userdetails.UserId;
                status = true;
                message = "Please provide registered email";
                return Json(new { status = status, message = message, id = usid }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "Please provide registered email";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
            //if (wordId != null && wordId != string.Empty)
            //{
            //    Tuple<bool, string> result = _userRepo.deletWord(wordId);
            //    return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    status = false;
            //    message = "failed";
            //    return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            //}

        }


    }
}