﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Manager.Wordutopia;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Models.Database;

namespace TutionCloudWeb.Controllers.Wordutopia
{
    public class SharingController : BaseController
    {
        protected TutionCloudEntities TutionCloudEntities = new TutionCloudEntities();
        SharingManager SharingManager = new SharingManager();
    
        public ActionResult Sharings()
        {
            return View();
        }
        public ActionResult CreateaWordListToShare()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            //OnlineCommunityModel model = new OnlineCommunityModel();
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_FETCH_USER_TOTAL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
            
        }
        [HttpGet]
        public JsonResult GetAllUserLists()
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = TutionCloudEntities.tUsers.Select(x => new {x.Username, x.UserId}).ToList();
            
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CreateaWordListToShareSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = SharingManager.SendLIsts(model);

            return Json(results,JsonRequestBehavior.AllowGet); 
        }

        public ActionResult WordListSharedwithMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();            
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
        [HttpPost]
        public JsonResult GetCurrendItemWordListSharedwithMe(SharingModel values)
        {
            CreateAWordListToShareModel CreateAWordListToShareModel = new CreateAWordListToShareModel();
            var results =  TutionCloudEntities.CreateAWordListToShares.FirstOrDefault(i => i.CreatedOn == values.CreatedOn);
            
            CreateAWordListToShareModel.Password = results.Password;
            CreateAWordListToShareModel.TestID = results.TestID;
            CreateAWordListToShareModel.LastName = results.LastName;



            return Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Sharing_WordListSharedwithMeList(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_WordListSharedwithMe.cshtml", model);
        }

        [HttpPost]
        public ActionResult Sharing_Word_List_Pagination(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Sharing_Word_List_Pagination.cshtml", model);
        }

        public JsonResult DeleteWordListSharedwithMe(string CreatedOn)
        {
            try
            {
                if (CreatedOn != null && CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteCreateAWordLIstShare(CreatedOn);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex){
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult Save_CreateaWordListToShare(CreateAWordListToShareModel model)
        {
            return null;
        }

        public ActionResult WordListSharedbyMe()
        {
            return View();
        }
        
        public ActionResult CreateaNewTesttoShare()
        {
            return View();
        }
        public ActionResult ShareanArchivedTest()
        {
            return View();
        }
        public ActionResult TestSharedbyMe()
        {
            return View();
        }
        public ActionResult TestSharedWithMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
        public ActionResult ManageSharedTestStats()
        {
            return View();
        }
	}
}