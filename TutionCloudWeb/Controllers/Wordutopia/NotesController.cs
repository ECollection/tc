﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Manager.Wordutopia;
using TutionCloudWeb.Model.Wordutopia;

namespace TutionCloudWeb.Controllers.Wordutopia
{
    public class NotesController : Controller
    {
        NotesManager NotesManager = new NotesManager();
        /// <summary>
        /// Name : Notes
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Notes()
        {
            Notes model = new Notes();
            model.userId = SessionModel.UserId; ;
            model = await NotesManager.Notes(model);
            return View(model);
        }
        /// <summary>
        /// Name : refreshUserNoteList
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public ActionResult refreshUserNoteList(Notes model)
        {
            return PartialView("~/Views/Notes/_pv_Wordutopia_Note_List.cshtml", model);
        }

        /// <summary>
        /// Name : refreshUserNoteListPagination
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult refreshUserNoteListPagination(Notes model)
        {
            return PartialView("~/Views/Notes/_pv_Wordutopia_Note_List_Pagination.cshtml", model);
        }

        /// <summary>
        /// Name : NoteCount
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public string NoteCount(long userId, string searchText)
        {
            try
            {
                var count = NotesManager.NoteCount(userId,searchText);
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        /// <summary>
        /// Name : deletNote
        /// CreateBy : SIBI
        /// CreateON : 15/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public object deletNote(string noteId)
        {
            bool status = false;
            string message = "failed";
            if (noteId != null && noteId != string.Empty)
            {
                Tuple<bool, string> result = NotesManager.deletNote(noteId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }
	}
}