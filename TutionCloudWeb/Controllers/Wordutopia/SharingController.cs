﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Manager.Wordutopia;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Models.Database;
using TutionCloudWeb.Models.Repository;

namespace TutionCloudWeb.Controllers.Wordutopia
{
    public class SharingController : BaseController
    {
        protected TutionCloudEntities TutionCloudEntities = new TutionCloudEntities();
        SharingManager SharingManager = new SharingManager();
        private userRepository _userRepo = new userRepository();
        public ActionResult Sharings()
        {
            return View();
        }
        public ActionResult CreateaWordListToShare()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            //OnlineCommunityModel model = new OnlineCommunityModel();
            model.userId = _User.UserId;
            model.TestId = SharingManager.CreateWordListMaxId(_User.UserId.ToString());
            model.totalWordCount = TutionCloudEntities.SP_FETCH_USER_TOTAL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
            
        }
        [HttpGet]
        public JsonResult GetAllUserLists()
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = TutionCloudEntities.tUsers.Select(x => new {x.Username, x.UserId}).ToList();
            var results2 = results.OrderBy(z => z.Username);
            return Json(results2, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SortDropdownListBYCreateaWordListToShare(List<CreateAWordListToShareModel> model)
        {
            //List<CreateAWordListToShareModel> results = new List<CreateAWordListToShareModel>();
            //foreach (var a1 in model)
            //{
            //    CreateAWordListToShareModel CreateAWordListToShareModel = new CreateAWordListToShareModel();
            //    CreateAWordListToShareModel.UserID = a1.UserID;
            //    CreateAWordListToShareModel.UserName = a1.UserName;
            //    results.Add(CreateAWordListToShareModel);
            //}

            var results2 = model.OrderBy(z => z.UserName).ToList();
            return Json(results2, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CreateaWordListToShareSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = SharingManager.SendLIsts(model);

            return Json(results,JsonRequestBehavior.AllowGet); 
        }

        [HttpGet]
        public JsonResult UpdateaWordListToShareSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = SharingManager.UpdateLIsts(model);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult WordListSharedwithMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();            
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpPost]
        public JsonResult ShareActivateCheckTestSharedWithMe(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass model)
        {
            string a1 = model.userId.ToString();
            string asd = null;
            var results = _context.TestShareds.Where(

                            x => x.Password == model.Password &&

                            x.LastName == model.LastName &&

                            x.TestID == model.TestId &&

                            x.UserID == a1

                            ).FirstOrDefault();
            if (results != null)
            {
                DateTime sss = Convert.ToDateTime(results.CreatedOn);
                string dd = sss.ToString("dd");
                string MM = sss.ToString("MM");
                string yyyy = sss.ToString("yyyy");
                string hh = sss.ToString("hh");
                string mm = sss.ToString("mm");
                string tt = sss.ToString("tt");
                model.datestring = dd + "/" + MM + "/" + yyyy + " " + hh + ":" + mm + " " + tt;
                asd = sss.ToString("{0:dd/MM/yyyy hh:mm tt}");

                var Update = _context.TestShareds.Where(

                          x => x.Password == model.Password &&

                          x.LastName == model.LastName &&

                          x.TestID == model.TestId &&

                          x.UserID == a1

                          ).ToList();

                foreach (var a2 in Update)
                {
                    a2.Retrieve = true;
                    
                    _context.SaveChanges();
                }
            }
            else
            {
                model.datestring = "0";
            }

            

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ShareActivateCheck(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass model)
        {
            string a1 = model.userId.ToString();
            var results = _context.CreateAWordListToShares.Where(

                            x => x.Password == model.Password &&

                            x.LastName == model.LastName &&

                            x.TestID == model.TestId &&

                            x.UserID == a1

                            ).FirstOrDefault();
            if (results != null)
            {
                DateTime sss = Convert.ToDateTime(results.CreatedOn);
                string dd = sss.ToString("dd");
                string MM = sss.ToString("MM");
                string yyyy = sss.ToString("yyyy");
                string hh = sss.ToString("hh");
                string mm = sss.ToString("mm");
                string tt = sss.ToString("tt");
                model.datestring = dd+"/"+MM+"/"+yyyy+" "+hh+":"+mm+" "+tt;
                string asd = sss.ToString("{0:dd/MM/yyyy hh:mm tt}");
                //model.datestring = sss.ToString("dd/MM/yyyy hh:mm tt");
            }
            else
            {
                model.datestring = "0";
            }

            var Update = _context.CreateAWordListToShares.Where(

                            x => x.Password == model.Password &&

                            x.LastName == model.LastName &&

                            x.TestID == model.TestId &&

                            x.UserID == a1

                            ).ToList();

            foreach (var a2 in Update)
            {
                a2.Retrieve = true;
                _context.SaveChanges();
            }
             

            return Json(model, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult GetAllWordListSharedwithMe(SharingModel values)
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = SharingManager.GetAllWordListSharedwithMe(values.CreatedOn);
            return Json(results,JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetCurrendItemWordListSharedwithMe(SharingModel values)
        {
            CreateAWordListToShareModel CreateAWordListToShareModel = new CreateAWordListToShareModel();
            var results =  TutionCloudEntities.CreateAWordListToShares.FirstOrDefault(i => i.CreatedOn == values.CreatedOn);
            
            CreateAWordListToShareModel.Password = results.Password;
            CreateAWordListToShareModel.TestID = results.TestID;
            CreateAWordListToShareModel.LastName = results.LastName;



            return Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Sharing_WordListSharedwithMeList(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_WordListSharedwithMe.cshtml", model);
        }

        [HttpPost]
        public ActionResult Sharing_Word_List_Pagination(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopia/_pv_Sharing_Word_List_Pagination.cshtml", model);
        }

        public JsonResult DeleteWordListSharedwithMe(string CreatedOn)
        {
            try
            {
                if (CreatedOn != null && CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteCreateAWordLIstShare(CreatedOn);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex){
                return Json("2", JsonRequestBehavior.AllowGet);
            }
            
        }
        public JsonResult DeleteALLWordListSharedwithMe(string userId)
        {
            try
            {
                if (userId != null && userId != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteALLWordListSharedwithMe(userId);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }
        //WordListSharedwithbyWords
        public ActionResult WordListSharedwithbyWords(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_WordListSharedwithbyWords_COUNT(model.CreatedOn, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public string SharedWithByWordCount(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            try
            {
                var count = TutionCloudEntities.SP_WordListSharedwithbyWords_COUNT(model.CreatedOn, string.Empty).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        [HttpPost]
        public ActionResult refreshUserWordbyList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_WordListSharedwithbyWords_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserWordBYListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_SharedwithMeWords_List_Pagination.cshtml", model);
        }

        public ActionResult WordListSharedwithMeWords(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
        public ActionResult WordListSharedwithMeWords_Test(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT_Test(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }


        [HttpPost]
        public JsonResult checkIDforCreateAWordListToShare(TutionCloudWeb.Models.PostModel.OnlineCommunity.AddAllWord model)
        {
            try
            {
                long a1 = Convert.ToInt64(model.wordId);
                var results = TutionCloudEntities.tWords.Where(z => z.WordId == a1).FirstOrDefault();
                var result2 = TutionCloudEntities.tWords.Where(z => z.UserId == model.userId && z.Word == results.Word && z.IsActive == true ).FirstOrDefault();
                if (result2 == null)
                {
                    return Json("1",JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex){

                return Json("1", JsonRequestBehavior.AllowGet);
            }
           
        }

        public object deletWordSharedWithMeWords(string wordId)
        {//SP_deletWordSharedWithMeWords
            try
            {
                if (wordId != null && wordId != string.Empty)
                {
                    var results = TutionCloudEntities.SP_deletWordSharedWithMeWords(wordId);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult deletSelecteSharedWithMe(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            string[] values = model.wordIds.Split(',');
            try
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var results = TutionCloudEntities.SP_deletWordSharedWithMeWords(values[i].ToString());

                }
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            
        }
        [HttpPost]
        public ActionResult deletAllSharedWithMe(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            try
            {

                var results = TutionCloudEntities.DeleteCreateAWordLIstShare(model.CreatedOn);

                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        public string SharedWithMeWordCount(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            try
            {
                var count = _context.SP_SharedWithMeWord_COUNT(model.userId, model.searchText ?? string.Empty, model.CreatedOn).FirstOrDefault().ToString();
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        [HttpPost]
        public ActionResult refreshUserWordList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Wordutopia_Word_List.cshtml", model);
        }
        [HttpPost]
        public ActionResult refreshUserWordListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Sharing/_pv_Wordutopia_Word_List_Pagination.cshtml", model);
        }

        [HttpPost]
        public ActionResult addfavouriteword(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.addfavouriteword(model);
                return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditWordSharedWithMe(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            string[] values = strOriginal.Split('-');
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = values[i].Trim();
            }

            var wordId = Convert.ToInt64(values[0]);
            var model = new TutionCloudWeb.Models.PostModel.Word.EditWord();
            model.redirectTo = values[1];
            model.mainPanelIndex = Convert.ToInt32(values[2]);
            model.userId = _User.UserId;
            model.wordId = wordId;

            var word = _context.SP_FETCH_WORD_BY_ID(wordId).ToList().Select(z => new TutionCloudWeb.Models.DataClass.SP_FETCH_WORD_BY_ID(z)).FirstOrDefault();
            if (word != null)
            {
                model.sharOnline = word.SHAREONLINE;
                model.definition = word.DEFINITION;
                model.hint = word.HINT;
                model.phrase = word.PHRASE;
                model.pronounciation = word.PRONOUNCIATION;
                model.word = word.WORD;
                return View(model);
            }
            else
            {
                //return HttpNotFound();
                return RedirectToAction("WordListSharedwithMe");
            }

        }

        public ActionResult Save_CreateaWordListToShare(CreateAWordListToShareModel model)
        {
            return null;
        }

        public ActionResult WordListSharedbyMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList();
            //OnlineCommunityModel model = new OnlineCommunityModel();
            model.userId = _User.UserId;
            //model.TestId = "001";
            model.totalWordCount = TutionCloudEntities.SP_WordListSharedbyMe_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);            
        }
        
        public ActionResult CreateaNewTesttoShare()
        {
            var model = new TutionCloudWeb.Models.PostModel.Exam.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = _context.SP_FETCH_TOTAL_WORDLIST_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
        
        
        public ActionResult TestSharedWithMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordTestSharedWithMe_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }
      
        /// <summary>
        /// 30_04_2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SetTestFormat(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.Exam.CreateTest();
            model.testTime = "";
            model.timePerWord = "";
            model.title = "";
            model.userId = _User.UserId;
            model.wordIds = strOriginal;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult createtest(TutionCloudWeb.Models.PostModel.Exam.CreateTest model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = _userRepo.createtest(model);
                return Json(new { status = result.status, message = result.message, id = result.examId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddSharedTestUsers(string id)
        {
            byte[] b = Convert.FromBase64String(id);

            var strOriginal = System.Text.Encoding.UTF8.GetString(b);

            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            //model.userId = _User.UserId;
            //model.examId = strOriginal;
            var examId = Convert.ToInt64(strOriginal);
            var exam = _context.tExams.FirstOrDefault(z => z.ExamId == examId);
            if (exam != null)
            {
                model.TestId = SharingManager.ShareAnArchivedTestID(_User.UserId.ToString()).ToString();
                model.testTime = exam.TestTime;
                model.timePerWord = exam.TimePerWord;
                model.examId = strOriginal;
                model.userId = _User.UserId;

            }
            else
            {
                model.testTime = "0";
                model.timePerWord = "0";
            }
            return View(model);
        }
        [HttpPost]
        public JsonResult Sent_ShareAnArchivedTest(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass model)
        {
            string results = SharingManager.Sent_ShareAnArchivedTest(model);
            return Json(results,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShareanArchivedTest()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.ShareAnArchivedTest_Get_Set();
            model.UserId = _User.UserId;
            model.ShareanArchivedTest_Lists = SharingManager.setSortedItems(model);

            return View(model);
        }
             

        [HttpPost] 
        public JsonResult ShareanArchivedTest_Binding(SharingModel values)
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = SharingManager.ShareanArchivedTest_Binding(values);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteWordListAchivedTest_Delete(SharingModel model)
        {
            try
            {
                if (model.CreatedOn != null && model.CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteWordListAchivedTest_Delete(model.CreatedOn, model.userId ,model.TestID);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult WordListShareanArchivedTest_View(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_WordListShareAnArchivedTest_COUNT(model.CreatedOn, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        [HttpGet]
        public JsonResult TestSharedSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = SharingManager.TestSharedSendList(model);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestSharedbyMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList();
            //OnlineCommunityModel model = new OnlineCommunityModel();
            model.userId = _User.UserId;
            //model.TestId = "001";
            model.totalWordCount = TutionCloudEntities.SP_TestSharedSharedbyMe_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model); 
        }

        [HttpPost]
        public JsonResult GetAllWordListTestSharedwithMe(SharingModel values)
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = SharingManager.GetAllWordListTestSharedwithMe(values.CreatedOn);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UpdateaWordListToTestSharedbyMeSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = SharingManager.UpdateaWordListToTestSharedbyMeSendList(model);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteWordListTestSharedbyMeS(string CreatedOn)
        {
            try
            {
                if (CreatedOn != null && CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteTestSharedbyMe(CreatedOn);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult refreshUserWordTestShareList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_WordListTestSharedbyMe.cshtml", model);
        }
        [HttpPost]
        public ActionResult refreshUserWordTestShareListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Sharing/_pv_WordListTestSharedbyMe_Pagination.cshtml", model);
        }

        public ActionResult WordListSharedTestSharedbyMeWords(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_WordListSharedTestSharedbyMe_COUNT(model.CreatedOn, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public object deletWordSharedTestSharedbyMeWords(string wordId)
        {//SP_deletWordSharedWithMeWords
            try
            {
                if (wordId != null && wordId != string.Empty)
                {
                    var results = TutionCloudEntities.SP_deletWordSharedTestSharedbyMeWords(wordId);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult deletSelecteSharedestSharedbyMe(TutionCloudWeb.Models.PostModel.Word.DeleteSelectedWord model)
        {
            string[] values = model.wordIds.Split(',');
            try
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var results = TutionCloudEntities.SP_deletWordSharedTestSharedbyMeWords(values[i].ToString());

                }
                return Json("1", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult refreshUserTestSharedbyMeWordbyList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_WordListSharedTestSharedbyMe_List.cshtml", model);
        }

        [HttpPost]
        public ActionResult refreshUserTestSharedbyMeWordbyListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_SharedTestShare_List_Pagination.cshtml", model);
        }

        
        public JsonResult DeleteALLWordListTestSharedWithMe(string userId)
        {
            try
            {
                if (userId != null && userId != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteALLWordListTestSharedWithMe(userId);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult DeleteWordListTestSharedWithMeSS(string CreatedOn)
        {
            try
            {
                if (CreatedOn != null && CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.DeleteTestSharedWithMeTestSharedWithMe(CreatedOn);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Sharing_WordListTestSharedWithMeList(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordList model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_WordListTestSharedWithMe.cshtml", model);
        }

        [HttpPost]
        public ActionResult Sharing_WordListTestSharedWithMeList_Pagination(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordListPagination model)
        {
            return PartialView("~/Views/Sharing/_pv_Sharing_TestSharedWithMe_List_Pagination.cshtml", model);
        }

        public ActionResult WordListSharedOnlineUsers(string wordIds)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord();
            model.userId = _User.UserId;
            model.TestID = SharingManager.CreateWordListMaxId(_User.UserId.ToString());
            model.wordIds = wordIds;
            //model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public ActionResult ManageSharedTestStats()
        {
            return View();
        }
        public JsonResult FindSummaryResultForAnySharedTest(TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass model)
        {
            string a1 = _User.UserId.ToString();
            var results = _context.TestShareds.Where(

                            x => x.Password == model.Password &&

                            x.LastName == model.LastName &&

                            x.TestID == model.TestId &&

                            x.UserID == a1

                            ).FirstOrDefault();
            if (results != null)
            {
                var Update = _context.TestShareds.Where(

                               x => x.Password == model.Password &&

                               x.LastName == model.LastName &&

                               x.TestID == model.TestId &&

                               x.UserID == a1

                               ).ToList();

                foreach (var a2 in Update)
                {
                    a2.Retrieve = true;
                    _context.SaveChanges();
                }
            }
            else
            {
                return Json("0", JsonRequestBehavior.AllowGet);
            }

            
            return Json("1",JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestStatsForSpecificUsers(string SearchName)
        {
            ShareAnArchivedTestModel ShareAnArchivedTestModel = new ShareAnArchivedTestModel();
            List<ShareAnArchivedTestModel> model_Lists = new List<ShareAnArchivedTestModel>();
            var getId = _context.tUsers.Where(x => x.Username == SearchName && x.IsActive == true).FirstOrDefault();
            if(getId != null){
                string UserId =  getId.UserId.ToString();
                var getArchivedTests = _context.ShareAnArchivedTests.Where(x => x.SentUserID == UserId && x.IsActive == true).OrderByDescending(x => x.TestID).ToList();
                string testID = "";
                foreach (var a1 in getArchivedTests)
                {
                    ShareAnArchivedTestModel model = new ShareAnArchivedTestModel();
                    model.ShareAnArchivedTestId = a1.ShareAnArchivedTestId;
                    model.SearchName = SearchName;
                    model.SurName = getId.Surname;
                    model.WordId = a1.WordId;
                    model.ExamId = a1.ExamId;
                    if(model.ExamId != null){
                        long ExamId = (long)model.ExamId;
                        var a2 = _context.tExams.Where(x => x.ExamId == ExamId).FirstOrDefault();
                        model.Exam_TestTitle = a2.Title;
                        model.Exam_TestTime = a2.TestTime;
                        model.Exam_TimePerWord = a2.TimePerWord;
                    }
                    model.Guid = a1.Guid;
                    model.Password = a1.Password;
                    model.UserId = a1.UserId;
                    model.TestID = a1.TestID;
                    model.LastName = a1.LastName;
                    model.SentUserID = a1.SentUserID;
                    model.Word = a1.Word;
                    model.Pronounciation = a1.Pronounciation;
                    model.Phrase = a1.Phrase;
                    model.Hint = a1.Hint;
                    model.Definition = a1.Definition;
                    model.ShareOnline = a1.ShareOnline;
                    model.CreatedDate = a1.CreatedDate;
                    model.CreatedOn = a1.CreatedOn;
                    if (testID != a1.TestID)
                    {
                        model_Lists.Add(model);
                    }
                    
                    testID =  a1.TestID;
                }
                 
                ShareAnArchivedTestModel.ShareAnArchivedTestModel_Lists = model_Lists;

            }

            return View(ShareAnArchivedTestModel);
        }
        public ActionResult TestStatsForSpecificUsers_Views(string CreatedOn)
        {
            var model = new TutionCloudWeb.Models.PostModel.Word.WordClass();
            model.userId = _User.UserId;
            model.CreatedOn = CreatedOn;
            model.totalWordCount = TutionCloudEntities.SP_WordListShareAnArchivedTest_COUNT(model.CreatedOn, string.Empty).FirstOrDefault().ToString();
            return View(model);
            
        }

        public JsonResult ManageSharedTestStats_Delete(SharingModel model)
        {
            try
            {
                if (model.CreatedOn != null && model.CreatedOn != string.Empty)
                {
                    var results = TutionCloudEntities.ManageSharedTestStats_Delete(model.CreatedOn, model.userId, model.TestID);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return Json("0", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("2", JsonRequestBehavior.AllowGet);
            }

        }



	}
}