﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Manager.Wordutopia;
using TutionCloudWeb.Model.Wordutopia;
using TutionCloudWeb.Models.Database;

namespace TutionCloudWeb.Controllers.Wordutopia
{
    public class OnlineCommunityController : BaseController
    {
        protected TutionCloudEntities TutionCloudEntities = new TutionCloudEntities();
        OnlineCommunityManager OnlineCommunityManager = new OnlineCommunityManager();
        public ActionResult OnlineCommunity()
        {
            return View();
        }
        public ActionResult CreateaWordListToShare()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();
            //OnlineCommunityModel model = new OnlineCommunityModel();
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_FETCH_USER_TOTAL_WORD_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
            
        }
        [HttpGet]
        public JsonResult GetAllUserLists()
        {
            CreateAWordListToShareModel model = new CreateAWordListToShareModel();
            var results = TutionCloudEntities.tUsers.Select(x => new {x.Username, x.UserId}).ToList();
            
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CreateaWordListToShareSendList(TutionCloudWeb.Models.PostModel.Word.AddFavouriteWord model)
        {
            var results = OnlineCommunityManager.SendLIsts(model);

            return Json(results,JsonRequestBehavior.AllowGet); 
        }

        public ActionResult WordListSharedwithMe()
        {
            var model = new TutionCloudWeb.Models.PostModel.OnlineCommunity.WordClass();            
            model.userId = _User.UserId;
            model.totalWordCount = TutionCloudEntities.SP_CreateAWordListToShare_COUNT(model.userId, string.Empty).FirstOrDefault().ToString();
            return View(model);
        }

        public ActionResult Save_CreateaWordListToShare(CreateAWordListToShareModel model)
        {
            return null;
        }

        public ActionResult WordListSharedbyMe()
        {
            return View();
        }
        
        public ActionResult CreateaNewTesttoShare()
        {
            return View();
        }
        public ActionResult ShareanArchivedTest()
        {
            return View();
        }
        public ActionResult TestSharedbyMe()
        {
            return View();
        }
        public ActionResult TestSharedWithMe()
        {
            return View();
        }
        public ActionResult ManageSharedTestStats()
        {
            return View();
        }
	}
}