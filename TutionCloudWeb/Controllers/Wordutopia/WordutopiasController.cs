﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Manager.Wordutopia;
using TutionCloudWeb.Model.Wordutopia;


namespace TutionCloudWeb.Controllers.Wordutopia
{
    public class WordutopiasController : Controller
    {
        WordutopiaManager WordutopiaManager = new WordutopiaManager();

        /// <summary>
        /// Name : Words
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Words()
        {
            WordsModel model = new WordsModel();
            model.userId = SessionModel.UserId;
            model = await WordutopiaManager.Words(model);
            return View(model);
        }
        /// <summary>
        /// Name : CreateWord
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> CreateWord()
        {
            WordsModel model = new WordsModel();
            model.userId = SessionModel.UserId;
            model.sharOnline = true;
            return View(model);
        }

        /// <summary>
        /// Name : addword
        /// CreateBy : SIBI
        /// CreateON : 13/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult addword(WordsModel model)
        {
            bool status = true;
            string message = "success";
            if (ModelState.IsValid)
            {
                var result = WordutopiaManager.Addword(model);
                //return Json(new { status = result.status, message = result.message }, JsonRequestBehavior.AllowGet);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        /// <summary>
        /// Name : refreshUserWordList
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public ActionResult refreshUserWordList(TutionCloudWeb.Models.PostModel.Word.WordList model)
        {
            return PartialView("~/Views/Wordutopias/_pv_Wordutopia_Word_List.cshtml", model);
        }
        /// <summary>
        /// Name : refreshUserWordList
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult refreshUserWordListPagination(TutionCloudWeb.Models.PostModel.Word.WordListPagination model)
        {
            return PartialView("~/Views/Wordutopias/_pv_Wordutopia_Word_List_Pagination.cshtml", model);
        }
        /// <summary>
        /// Name : refreshUserWordList
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public object deletWord(string wordId)
        {
            bool status = false;
            string message = "failed";
            if (wordId != null && wordId != string.Empty)
            {
                Tuple<bool, string> result = WordutopiaManager.deletWord(wordId);
                return Json(new { status = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                status = false;
                message = "failed";
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Name : WordCount
        /// CreateBy : SIBI
        /// CreateON : 14/03/2019
        /// UpdatedBy : non
        /// Details : non 
        /// </summary>
        /// <returns></returns>
        public string WordCount(long userId, string searchText)
        {
            try
            {
                var count = WordutopiaManager.WordCount(userId, searchText);
                return count;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        

	}
}