﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TutionCloudWeb.Models.Database;

namespace TutionCloudWeb.Controllers
{
    public class BaseController : Controller
    {
        protected TutionCloudEntities _context = new TutionCloudEntities();
        public DateTime currentTime = System.DateTime.UtcNow;

        protected TutionCloudWeb.Models.Database.tUser _User;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //check whether the user is logged in or else
            if (User != null && User.Identity.IsAuthenticated)
            {
                string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                var userId = long.Parse(User.Identity.Name);
                var user = getUserById(userId);
                Session["User"] = user;
                _User = (tUser)Session["User"];
                //filterContext.Result = new RedirectResult("/Admin/home");
            }
            else
            {
                filterContext.Result = new RedirectResult("/User/Login");
            }
        }
        public tUser getUserById(long Id)
        {
            return _context.tUsers.Where(z => z.UserId == Id).FirstOrDefault();
        }
    }
}