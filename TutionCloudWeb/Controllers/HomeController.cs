﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TutionCloudWeb.Controllers
{
    public class HomeController : PreLoginBaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Wordutopia()
        {
         
            return View();
        }

        public ActionResult Quester()
        {
            return View();
        }

        public ActionResult Choicer()
        {
            return View();
        }

    }
}