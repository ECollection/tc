//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TutionCloudWeb.Models.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class tExam
    {
        public tExam()
        {
            this.tExamResults = new HashSet<tExamResult>();
            this.tExamWords = new HashSet<tExamWord>();
        }
    
        public long ExamId { get; set; }
        public System.Guid ExamGuid { get; set; }
        public string Title { get; set; }
        public string TestTime { get; set; }
        public string TimePerWord { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long UserId { get; set; }
    
        public virtual tUser tUser { get; set; }
        public virtual ICollection<tExamResult> tExamResults { get; set; }
        public virtual ICollection<tExamWord> tExamWords { get; set; }
    }
}
