﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public class CreateAWordListToShareModel
    {
        public decimal CreateAWordListToShareID { get; set; }
        public string SentUserID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string WordId { get; set; }
        public string Password { get; set; }
        public string TestID { get; set; }
        public string LastName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedOn { get; set; }
        public string Pronounciation { get; set; }
        public string Phrase { get; set; }
        public string Hint { get; set; }
        public string Definition { get; set; }
        public Nullable<bool> ShareOnline { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public Nullable<System.Guid> Guid { get; set; }
        public string Word { get; set; }

        //public decimal CreateAWordListToShareID { get; set; }
        //public string UserID { get; set; }
        //public string UserName { get; set; }
        //public string Words { get; set; }
        //public string Password { get; set; }
        //public string TestID { get; set; }
        //public string LastName { get; set; }
        //public Nullable<bool> IsActive { get; set; }
        //public string CreatedOn { get; set; }
        public List<CreateAWordListToShareModel> CreateAWordListToShareModelList { get; set; }

        public List<CreateAWordListToShareModel> GetallItems { get; set; }
        public List<CreateAWordListToShareModel> GetAllUserLists { get; set; }

        public Nullable<bool> Retrieve { get; set; }
       
    }
}