﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public class Notes
    {            
                [Required(ErrorMessage = "required")]
                public string title { get; set; }
                [Required(ErrorMessage = "required")]
                public string description { get; set; }  
                public bool status { get; set; }
                public string message { get; set; }            
                public int index { get; set; }             
                public string totalNoteCount { get; set; }
                public string searchText { get; set; }
                public string sortBy { get; set; }
                public string sortOrder { get; set; }
                [Required(ErrorMessage = "required")]
                public long userId { get; set; }
                [Required(ErrorMessage = "required")]
                public long noteId { get; set; }
                public int Noteid { get; set; }              
                public string Title { get; set; }
                public string Message { get; set; }
        
    }
}