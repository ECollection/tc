﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public static class staticVerifyCode
    {
        public static string VerifyCode{get; set;}
    }
    public class UserModel
    {
        [Required(ErrorMessage = "required")]
        public string forename { get; set; }

        [Required(ErrorMessage = "required")]
        public string surname { get; set; }

        [Required(ErrorMessage = "required")]
        public string username { get; set; }

        [Required(ErrorMessage = "required")]
        [StringLength(255, ErrorMessage = "Must be between 8 and 15 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "required")]
        [StringLength(255, ErrorMessage = "Must be between 8 and 15 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Compare("password")]
        public string confirmPassword { get; set; }
        public string webname { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public string VerifyCode { get; set; }
        public string RandomNumber { get; set; }

        
    }
}