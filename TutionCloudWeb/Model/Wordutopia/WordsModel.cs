﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public class WordsModel
    {
        public long userId { get; set; }
        public string totalWordCount { get; set; }
        public int index { get; set; }        
        public string searchText { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public string Message { get; set; }


        [Required(ErrorMessage = "required")]
        public string word { get; set; }
        [Required(ErrorMessage = "required")]
        public string pronounciation { get; set; }
        [Required(ErrorMessage = "required")]
        public string phrase { get; set; }
        [Required(ErrorMessage = "required")]
        public string hint { get; set; }
        [Required(ErrorMessage = "required")]
        public string definition { get; set; }
        public bool sharOnline { get; set; }

        public bool status { get; set; }
        public string message { get; set; }

    }
}