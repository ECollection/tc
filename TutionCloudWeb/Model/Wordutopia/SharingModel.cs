﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public class SharingModel
    {
        public long userId { get; set; }
        public string totalWordCount { get; set; }
        public string CreatedOn { get; set; }
        public int shared {get;set;}
        public string TestID { get; set; }

    }
}