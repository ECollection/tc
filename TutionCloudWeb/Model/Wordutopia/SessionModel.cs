﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public static class SessionModel
    {
        public static long UserId { get; set; }
        public static System.Guid UserGuid { get; set; }
        public static string Forename { get; set; }
        public static string Surname { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string Webname { get; set; }
        public static System.DateTime AccountCreatedDate { get; set; }
        public static bool IsActive { get; set; }
        public static bool IsSuspended { get; set; }
        public static bool PushStatus { get; set; }
        public static System.DateTime ProfileLastEditedDate { get; set; }
        public static string ProfileImage { get; set; }
        public static string email { get; set; }
    }
}