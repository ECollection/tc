﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TutionCloudWeb.Model.Wordutopia
{
    public class ShareAnArchivedTestModel
    {
        public long ShareAnArchivedTestId { get; set; }
        public Nullable<long> WordId { get; set; }
        public Nullable<long> ExamId { get; set; }
        public Nullable<System.Guid> Guid { get; set; }
        public Nullable<long> UserId { get; set; }
        public string Password { get; set; }
        public string TestID { get; set; }
        public string LastName { get; set; }
        public string SentUserID { get; set; }
        public string Word { get; set; }
        public string Pronounciation { get; set; }
        public string Phrase { get; set; }
        public string Hint { get; set; }
        public string Definition { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> ShareOnline { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string CreatedOn { get; set; }
        public string SearchName { get; set; }
        public string SurName { get; set; }
        public List<ShareAnArchivedTestModel> ShareAnArchivedTestModel_Lists { get; set; }
        public string Exam_TestTitle { get; set; }
        public string Exam_TestTime { get; set; }
        public string Exam_TimePerWord { get; set; }
    }
}